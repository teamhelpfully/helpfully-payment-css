var gulp = require('gulp'),
		s3 = require('gulp-s3-upload')({
	key:       '',
	secret:    ''
});

gulp.task("publish", function() {
	gulp.src("../stylesheets/")
			.pipe(s3({
				bucket: 'helpfully.dk', //  Required
				acl:    'public-read'       //  Optional ACL permissions, defaults to public-read.
			}))
	;
});

//publish to s3 bucket
gulp.task('publish', ['publish']);